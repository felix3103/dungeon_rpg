/* 
 * Inhalt / Content	: 	Einstellungen der GUI vom Hauptfenster des Spiels / File of the Maingame GUI
 * 
 * Autor  / Creator	: 	Janek Hickethier
 * 
 * Datum  / Date	: 	28.06.2020
 * 
 * Version			: 	1.00
 * 	
 * */
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Image;

/*
 * Erstelle die Klasse "MainGame"
 * */
public class MainGame extends JFrame {
	
	/*
 	 * Attribute
 	 * */
	public Panel_Charw panelCharw;
	public Panel_Game panelGame;
	public JPanel panel_help;
	public JPanel panel_saveandexit;
	
	private JPanel contentPane;
	private JPanel panel_Menu;
	private JLabel lbl_logo;
	private JLabel lbl_help;
	private JLabel lbl_saveandexit;
	private JPanel panel_content;
	private Image imglogo = new ImageIcon(MainGame.class.getResource("/images/dragon.png")).getImage().getScaledInstance(90, 90, Image.SCALE_SMOOTH);
	
	/*
	 * Erstellen des "MainGame"-Fensters
	 * */
	public MainGame(MouseAdapter main, MouseAdapter game, MouseAdapter character) {
		
		/*
		 * GUI-Informationen über das ganze Fenster
		 * */
		setTitle("Dungeon RPG");
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainGame.class.getResource("/images/dragon.png")));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 550);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setForeground(Color.BLACK);
		contentPane.setBackground(new Color(0, 51, 51));
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		panelCharw = new Panel_Charw(character);
		panelGame = new Panel_Game(game);
		
			/*
			 * Das Panel dient als Menü-Fenster für das Spiel
			 * */
			panel_Menu = new JPanel();
			panel_Menu.setLayout(null);
			panel_Menu.setBorder(new LineBorder(new Color(0, 0, 0)));
			panel_Menu.setBackground(new Color(255, 222, 173));
			panel_Menu.setBounds(10, 1, 200, 518);
			contentPane.add(panel_Menu);
		
				/*
				 * Anzeigen des Logos
				 * */
				lbl_logo = new JLabel("");
				lbl_logo.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_logo.setBounds(10, 11, 186, 129);
				lbl_logo.setIcon(new ImageIcon(imglogo));
				panel_Menu.add(lbl_logo);
				
				/*
				 * Wenn sich die Maus auf diesem Panel befindet wird eine Hilfestellung angezeigt
				 * */	
				panel_help = new JPanel();
				panel_help.setLayout(null);
				panel_help.setBorder(new LineBorder(new Color(222, 184, 135)));
				panel_help.setBackground(new Color(210, 180, 140));
				panel_help.setBounds(1, 150, 198, 40);
				panel_Menu.add(panel_help);
					
					lbl_help = new JLabel("Hilfe");
					lbl_help.setHorizontalAlignment(SwingConstants.CENTER);
					lbl_help.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
					lbl_help.setBounds(10, 10, 178, 18);
					panel_help.add(lbl_help);
					panel_help.addMouseListener(main);
				
				/*
				 * Klickbares Panel um zu Speichern und um das Fenster zu schließen
				 * */
				panel_saveandexit = new JPanel();
				panel_saveandexit.setLayout(null);
				panel_saveandexit.setBorder(new LineBorder(new Color(222, 184, 135)));
				panel_saveandexit.setBackground(new Color(210, 180, 140));
				panel_saveandexit.setBounds(1, 449, 198, 40);
				panel_Menu.add(panel_saveandexit);
				
					lbl_saveandexit = new JLabel("Speichern und Verlassen");
					lbl_saveandexit.setHorizontalAlignment(SwingConstants.CENTER);
					lbl_saveandexit.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
					lbl_saveandexit.setBounds(10, 10, 178, 18);
					panel_saveandexit.add(lbl_saveandexit);	
					panel_saveandexit.addMouseListener(main);
			/*
			 * Dient zum einbinden der Panel "panelGame" und "panelCharw"
			 * */		
			panel_content = new JPanel();
			panel_content.setBorder(new LineBorder(new Color(0, 0, 0)));
			panel_content.setBackground(new Color(0, 51, 51));
			panel_content.setBounds(210, 1, 586, 518);
			contentPane.add(panel_content);
			panel_content.setLayout(null);
			panel_content.add(panelGame);
			panel_content.add(panelCharw);
			
			panelCharw.setVisible(true);
			panelGame.setVisible(true);
	}

	
/********************************** Funktionen **********************************/
	
	/*
	 * Deaktivieren des Game-Panels
	 * Aktivieren des Charakterwahl-Panels
	 * */
	public void pcharw() {
		panelGame.setVisible(false);
		panelCharw.setVisible(true);
	}
	
	/*
	 * Aktivieren des Game-Panels
	 * Deaktivieren des Charakterwahl-Panels
	 * */
	public void pgame() {
		panelGame.setVisible(true);
		panelCharw.setVisible(false);
	}
	
	/*
	 * Anzeigen des Errortextes bei falschen Nutzereingaben
	 * */
	public void setUserError(String text) {
		panelGame.lbl_usererror.setText(text);
	}
	
	/*
	 * Anzeigen des Textes aus der "Story.xml"
	 * */
	public void setStoryText(String text) {
		panelGame.txtpn_storytextfield.setText(text);
	}
	
	/*
	 * Auslesen des vom Spieler eingetragenen Textes
	 * */
	public String getUserInput() {
		return panelGame.textField_userinput.getText();
	}
		
	/*
	 * Leeren des Textfeldes "userinput" und des Labels "usererror"
	 * */
	public void ClearUserInput() {
		panelGame.textField_userinput.setText("");
	}
	public void ClearUserError() {
		panelGame.lbl_usererror.setText("");
	}
	
	/*
	 * Deaktivieren / aktivieren des Textfeldes "userinput"
	 * */
	private void hidUserInput() {
		panelGame.textField_userinput.setEditable(false);
	}
	private void visUserInput() {
		panelGame.textField_userinput.setEditable(true);
	}
	
	/*
	 * Wechsle den Text des "lbl_ok" von "Weiter" zu "Ok"
	 * Aktivieren des Labels "userinput"
	 * */
	public void NexttoOk() {
		panelGame.lbl_ok.setText("Ok");
		visUserInput();
	}
	
	/*
	 * Wechsle den Text des "lbl_ok" von "Ok" zu "Weiter"
	 * Zeige eine Nachricht im Label "usererror" wenn der Text auf "Weiter" steht
	 * Deaktivieren des Labels "userinput"
	 * */
	public void OktoNext() {
		panelGame.lbl_ok.setText("Weiter");
		panelGame.lbl_usererror.setText("Klicken Sie auf Weiter!");
		hidUserInput();
	}
	
	/*
	 * Umwandeln von Integer in einen String
	 * */
	public void showcharacter_mage(Character.Hero hero) {
		String strength = String.valueOf(hero.getStrength());
		String health = String.valueOf(hero.getHealth());
		panelCharw.showcharacter_mage(hero.getName(), strength, health);
	}
	
	public void showcharacter_traveller(Character.Hero hero) {
		String strength = String.valueOf(hero.getStrength());
		String health = String.valueOf(hero.getHealth());
		panelCharw.showcharacter_traveller(hero.name, strength, health);
	}
	public void showcharacter_valkyrie(Character.Hero hero) {
		String strength = String.valueOf(hero.getStrength());
		String health = String.valueOf(hero.getHealth());
		panelCharw.showcharacter_valkyrie(hero.name, strength, health);
	}
	
	/*
	 * Erzeugen eines Hover-Effektes
	 * */
	public void hoverenter_saveandexit() {
		panel_saveandexit.setBackground(new Color(230, 200, 160));
	}
	public void hoverexited_saveandexit() {
		panel_saveandexit.setBackground(new Color(210, 180, 140));
	}
	
	public void hoverenter_help() {
		panel_help.setBackground(new Color(230, 200, 160));
	}
	public void hoverexited_help() {
		panel_help.setBackground(new Color(210, 180, 140));
	}
	
	public void content_hoverenter_pmage() {
		panelCharw.panel_mage.setBackground(new Color(0,41,41));
	}
	public void content_hoverexited_pmage() {
		panelCharw.panel_mage.setBackground(new Color(0,31,31));
	}
	
	public void content_hoverenter_ptrav() {
		panelCharw.panel_traveller.setBackground(new Color(0,41,41));
	}
	public void content_hoverexited_ptrav() {
		panelCharw.panel_traveller.setBackground(new Color(0,31,31));
	}
	
	public void content_hoverenter_pvalk() {
		panelCharw.panel_valkyrie.setBackground(new Color(0,41,41));
	}
	public void content_hoverexited_pvalk() {
		panelCharw.panel_valkyrie.setBackground(new Color(0,31,31));
	}
	
	public void content_hoverenter_pok() {
		panelGame.panel_ok.setBackground(new Color(0,41,41));
	}
	public void content_hoverexited_pok() {
		panelGame.panel_ok.setBackground(new Color(0,31,31));
	}
	
	/*
	 * Prüfe ob der Text im "lbl_ok" "Ok" oder "Weiter" entspricht
	 * */
	public boolean check_ok() {
		return (panelGame.lbl_ok.getText().equals("Ok"));
	}
	public boolean check_next() {
		return (panelGame.lbl_ok.getText().equals("Weiter"));
	}
	
	/*
	 * Einbinden einer Nachrichten-Box / Pop-up Box
	 * */
	public void msgbox(String s) {
		JOptionPane.showMessageDialog(null, s);
	}
}