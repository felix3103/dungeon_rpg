/*
 * Inhalt / Content	:	StoryKlasse als LinkedList implementiert / Class Story implemented as LinkedList
 * 
 * Author / Creator	:	Felix Ohlendorf
 * 
 * Datum / Date		:	22.05.2020
 * 
 * Version			:	1.00
 * 
 */
import java.util.Iterator;
import java.util.LinkedList;
/*
 * Die Klasse "Story" ist als "LinkedList" realisiert. Dabei kann sich der Charakter (theoretisch) beliebig in der Story bewegen.
 * Dazu kann die Story variable angepasst werden.
 * 
 * */
public class Story extends LinkedList<Event>{

	/*
	 * "initEvents" initialisiert alle Events der Liste.
	 * Dabei wird geprüft, ob die Parameter leer sind. und die Antworten und Namen von leerzeichen befreit.
	 * 
	 * */
	public void initEvents(String[] name, String[] exercise, String[] answer, String[] help) {
		for (int i = 0; i < name.length; i++) {
			try {
				String stranswer = answer[i].trim();
				String strname = name[i].trim();
				if(strname == null) { strname = "leererName"; }
				if(exercise[i] == null) { exercise[i] = "leereAufgabe"; }
				if(stranswer == null) { stranswer = ""; }
				if(help[i] == null) { help[i] = ""; }
				add(new Event(strname, exercise[i], stranswer, help[i]));
			} catch(Exception e) {
				e.getStackTrace();
			}
		}
		return;
	}
	/*
	 * Mit "forward" wird in der Story ein Event weitergegangen.
	 * 
	 * */
	public Event forward(Event active) {
		int help = 0;
		try {
			if(active == null || active == getLast()) {
				help = indexOf(getFirst());
			}else{
				help = indexOf(active);
			}
		}catch(Exception e) {
			e.getStackTrace();
		}
		return get(help + 1);
	}
	/*
	 * "searchEvent" sucht nach dem Event mit dem Parameter "name".
	 * 
	 * */
	public int searchEvent(String name) {
		Iterator<Event> event = iterator();
		Event search = getFirst();
		try {
			while(search != getLast()) {
				if(search.getName().equals(name)) break;
				search = event.next();
			}
		}catch(Exception e) {
			e.getStackTrace();
		}
		return indexOf(search);
	}
	/*
	 * "randomObjekt" wählt aus dem Sortiment ein zufälliges Item aus und gibt dieses zurück.
	 * 
	 * */
	public Item randomObjekt() {
		double random = Math.random()*10;
		int index = (int)random;
		Item[] items = new Item[3];
		items[0] = new Item("Steak", 3);
		items[1] = new Item("Zauberstab", 5);
		items[2] = new Item("Schwert", 10);
		return items[index%3];
	}
}