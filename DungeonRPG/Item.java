/*
 * Inhalt / Content	:	ObjektKlasse für "Items" / Objectclass for "Items"
 * 
 * Autor  / Creator	:	Felix Ohlendorf
 * 
 * Datum  / Date	:	15.05.2020
 * 
 * Version:	1.00
 * 
 * */
public class Item{
	/*
	 * Attribute
	 * */
	private int health;
	private int strenght;
	private String name;
	/*
	 * Konstruktoren
	 * */
	public Item(String name){
		this.name = name;
		this.health = 0;
		this.strenght = 0;
	}
	
	public Item(String name, int health){
		this.name = name;
		this.health = health;
		this.strenght = 0;
	}
	
	public Item(String name, int health, int strenght){
		this.name = name;
		this.health = health;
		this.strenght = strenght;
	}
	
	/*
	 * Getter und Setter
	 * */
	public void setName(String name){ this.name = name; }
	public void setHealth(int health){ this.health = health; }
	public void setStrenght(int strenght){ this.strenght = strenght; }
	
	public String getName(){ return this.name; }
	public int getHealth(){ return this.health; }
	public int getStrenght(){ return this.strenght; }
}