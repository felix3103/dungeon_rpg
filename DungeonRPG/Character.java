/* 
 * Inhalt / Content	:	Character Klassen Heros und Enemies
 * 
 * Autor  / Creator	:	Felix Ohlendorf
 * 
 * Datum  / Date	:	30.05.2020
 * 
 * Version			:	1.00
 * 
 * */
import java.io.Serializable;
/*
 * Oberklasse "Character"
 * 
 * */
public class Character implements Serializable{
	/*
	 * Attribute
	 * 
	 * Ein Charakter sollte einen Namen(name), eine Stärke(strength) und Leben(health) besitzen.
	 * 
	 * */
	String name;
	int strength;
	int health;
	/*
	 * Getter und Setter
	 * 
	 * Mit den Gettern und Settern kann man jederzeit auf die Attribute eines Charakters zugreifen.
	 * 
	 * */
	public void setName(String name){ this.name = name; }
	public void setStrength(int strength){ this.strength = strength; }
	public void setHealth(int health){ this.health = health; }
	public String getName(){ return this.name; }
	public int getStrength(){ return this.strength; }
	public int getHealth(){ return this.health; }
	/* 
	 * Wir haben im Spiel 2 Arten von Charakteren, auf der einen Seite die Heros, mit denen der Spieler spielen kann.
	 * Auf der anderen die Gegener, die bei Kampf-Events erzeugt werden und die der Spieler mit seinem Helden besiegen muss.
	 * 
	 * Da die Helden und Gegener die selben Attribute besitzen habe ich die Entscheidung getroffen,
	 * diese als Unterklasse in die Oberklasse Charaktere zu stellen.
	 * Dabei erben die Unterklassen von der Oberklasse.
	 * 
	 * */
	
	/*
	 * Unterklasse "Hero"
	 * 
	 * */
	static class Hero extends Character{
		/*
		 * Die Konstruktoren für die Helden werden nur intern benötigt und sind daher auf private gesetzt.
		 * Dabei muss einem Helden jeder Wert einmal zugewiesen werden.
		 * 
		 * */
		private Hero (String name, int strength, int health){
			this.name = name;
			this.strength = strength;
			this.health = health;
		}

		private Hero(){}
		/*
		 * Zu beginn haben wir 3 Standard-Helden festgelegt.
		 * Die Funtion initalisiert alle Charaktere und gibt den Charakter mit dem angegebenen String "name" zurück.
		 * Damit sind die Helden zentral abgespeichert und es kann jederzeit auf die Funktion zugegriffen werden.
		 * 
		 * */
		public static Hero initHeros(String name){
			Hero[] hero = new Hero[3];
			hero[0] = new Hero("Walk\u00fcre", 10, 30);
			hero[1] = new Hero("Magier", 4, 40);
			hero[2]= new Hero("Reisender", 6, 35);
			Hero character = new Hero();
			for(int i = 0; i < hero.length; i++){
				if(name.toLowerCase().equals(hero[i].getName().toLowerCase())){
					character = hero[i];
				}
			}
			return character;
		}
	}

	/* 
	 * Unterklasse "Enemie"
	 * 
	 * */
	static class Enemie extends Character{
		/*
		 * Die Konstruktoren für die Gegner werden nur intern benötigt und sind daher auf private gesetzt.
		 * Dabei muss einem Gegner jeder Wert einmal zugewiesen werden.
		 * 
		 * */
		private Enemie(String name, int strength, int live){
			this.name = name;
			this.strength = strength;
			this.health = live;
		}
		private Enemie(){}
		/*
		 * Mit der Story haben wir auch Gegner festgelegt.
		 * Die Funtion initalisiert alle Charaktere und gibt den Charakter mit dem angegebenen String "name" zurück.
		 * Damit sind die Gegner zentral abgespeichert und es kann jederzeit auf die Funktion zugegriffen werden.
		 * 
		 * */
		public static Enemie initEnemie(String name){
			Enemie[] enemie = new Enemie[3];
			enemie[0] = new Enemie("wildschwein", 5, 25);	
			enemie[1] = new Enemie("troll", 6, 25);		
			enemie[2]= new Enemie("geist", 4, 25);
			Enemie character = new Enemie();
			character = enemie[2];
			for(int i = 0; i < enemie.length; i++){
				if(name.toLowerCase().equals(enemie[i].getName().toLowerCase())){
					character = enemie[i];
				}
			}
			return character;
		}
		
	}
}