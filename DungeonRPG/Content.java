/*
 * Inhalt / Content   :    Reader für die Texte aus der XML Datei / Reader of the texts from the XML file
 *
 * Author / Creator   :    Georg Reichelt
 *
 * Datum / Date       :    22.06.2020
 *
 * Version	          :    1.00
 *
 * */
import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Content {
    int length = 7;
    /*
     * Attribute
     * */
	public String[] id = new String[length];
	public String[] name = new String[length];
	public String[] exercise = new String[length];
	public String[] iqplus = new String[length];
	public String[] answer = new String[length];
	public String[] end = new String[length];

   public static Content initStory() {
	   
	   Content content = new Content();
	   
      try {
         /*
          * path xml file
          * */
         File inputFile = new File("stories/story.xml");
         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.parse(inputFile);
         doc.getDocumentElement().normalize();
         NodeList nList = doc.getElementsByTagName("textinhalt");
         /*
          * Liest jedes Attribut über einen Nametag aus und weist diesem einem Attribut zu
          * */
         for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               content.id[temp] = (
                  eElement.getAttribute("id"));
               content.name[temp] = (
                  eElement
                  .getElementsByTagName("titel")
                  .item(0)
                  .getTextContent());
               content.exercise[temp] = (
                  eElement
                  .getElementsByTagName("eintragstext")
                  .item(0)
                  .getTextContent()
                  + eElement
                  .getElementsByTagName("quiz")
                  .item(0)
                  .getTextContent());
               content.iqplus[temp] = (
                  eElement
                  .getElementsByTagName("iqplus")
                  .item(0)
                  .getTextContent());
               content.answer[temp] = (
                  eElement
                  .getElementsByTagName("antwort")
                  .item(0)
                  .getTextContent());
               content.end[temp] = (
                  eElement
                  .getElementsByTagName("ende")
                  .item(0)
                  .getTextContent());
            }
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
      return content;
   }
}