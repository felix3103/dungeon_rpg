/* 
 * Inhalt / Content	: 	Einstellungen der GUI von der Charakterwahl / File of the characterchoice GUI
 * 
 * Autor  / Creator	: 	Janek Hickethier
 * 
 * Datum  / Date	: 	28.06.2020
 * 
 * Version			: 	1.00
 * 	
 * */

import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import javax.swing.border.LineBorder;

/*
 * Erstelle die Klasse "Panel_charw
 * */
public class Panel_Charw extends JPanel {
	
	/*
	 * Attribute
	 * */
	public JPanel panel_mage;
	public JPanel panel_traveller;
	public JPanel panel_valkyrie;
	
	private JLabel lbl_name_mage;
	private JLabel lbl_icon_mage;
	private JLabel lbl_strength_mag;
	private JLabel lbl_strength_mage;
	private JLabel lbl_health_mag;
	private JLabel lbl_health_mage;	
	private JLabel lbl_name_traveller;
	private JLabel lbl_icon_traveller;
	private JLabel lbl_strength_trav;
	private JLabel lbl_strength_traveller;
	private JLabel lbl_health_trav;	
	private JLabel lbl_health_traveller;
	private JLabel lbl_name_valkyrie;
	private JLabel lbl_icon_valkyrie;
	private JLabel lbl_strength_val;
	private JLabel lbl_strength_valkyrie;
	private JLabel lbl_health_val;
	private JLabel lbl_health_valkyrie;	
	private Image img_mage = new ImageIcon(Panel_Charw.class.getResource("/images/magier.png")).getImage().getScaledInstance(60, 60, Image.SCALE_SMOOTH);
	private Image img_traveller = new ImageIcon(MainGame.class.getResource("/images/don-quijote.png")).getImage().getScaledInstance(60, 60, Image.SCALE_SMOOTH);
	private Image img_valkyrie = new ImageIcon(MainGame.class.getResource("/images/walkuere.png")).getImage().getScaledInstance(60, 60, Image.SCALE_SMOOTH);

	/*
	 * Erstellen des Panels "Panel_Charw"
	 * */
	public Panel_Charw(MouseAdapter ma) {
		
		/*
		 * GUI-Informationen über das ganze Panel
		 * */
		setBackground(new Color(0, 51, 51));
		setBounds(0, 0, 584, 521);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setLayout(null);
		
			/*
			 * Panel für die Informationen des Charakters "Magier"
			 * */
			panel_mage = new JPanel();
			panel_mage.setBorder(new LineBorder(new Color(0, 0, 0)));
			panel_mage.setBounds(35, 50, 500, 120);
			panel_mage.setBackground(new Color(0, 31, 31));
			panel_mage.addMouseListener(ma);
			panel_mage.setLayout(null);
			add(panel_mage);
			
				lbl_name_mage = new JLabel("Platzhalter");
				lbl_name_mage.setForeground(Color.WHITE);
				lbl_name_mage.setFont(new Font("Bookman Old Style", Font.PLAIN, 15));
				lbl_name_mage.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_name_mage.setBounds(137, 10, 355, 30);
				panel_mage.add(lbl_name_mage);
				
				lbl_icon_mage = new JLabel("");
				lbl_icon_mage.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_icon_mage.setBounds(40, 30, 60, 60);
				lbl_icon_mage.setIcon(new ImageIcon(img_mage));
				panel_mage.add(lbl_icon_mage);
				
				lbl_strength_mag = new JLabel("St\u00e4rke");
				lbl_strength_mag.setForeground(Color.WHITE);
				lbl_strength_mag.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_strength_mag.setFont(new Font("Bookman Old Style", Font.PLAIN, 13));
				lbl_strength_mag.setBounds(160, 60, 110, 15);
				panel_mage.add(lbl_strength_mag);
				
				lbl_strength_mage = new JLabel("Platzhalter");
				lbl_strength_mage.setForeground(Color.WHITE);
				lbl_strength_mage.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_strength_mage.setFont(new Font("Bookman Old Style", Font.PLAIN, 11));
				lbl_strength_mage.setBounds(160, 86, 110, 15);
				panel_mage.add(lbl_strength_mage);
				
				lbl_health_mag = new JLabel("Leben");
				lbl_health_mag.setForeground(Color.WHITE);
				lbl_health_mag.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_health_mag.setFont(new Font("Bookman Old Style", Font.PLAIN, 13));
				lbl_health_mag.setBounds(350, 60, 110, 15);
				panel_mage.add(lbl_health_mag);
				
				lbl_health_mage = new JLabel("Platzhalter");
				lbl_health_mage.setForeground(Color.WHITE);
				lbl_health_mage.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_health_mage.setFont(new Font("Bookman Old Style", Font.PLAIN, 11));
				lbl_health_mage.setBounds(350, 86, 110, 15);
				panel_mage.add(lbl_health_mage);
			
		    /*
			 * Panel für die Informationen des Charakters "Reisender"
			 * */
			panel_traveller = new JPanel();
			panel_traveller.setBackground(new Color(0, 31, 31));
			panel_traveller.setBorder(new LineBorder(new Color(0, 0, 0)));
			panel_traveller.setLayout(null);
			panel_traveller.addMouseListener(ma);
			panel_traveller.setBounds(35, 199, 500, 120);
			add(panel_traveller);
			
				lbl_name_traveller = new JLabel("Platzhalter");
				lbl_name_traveller.setForeground(Color.WHITE);
				lbl_name_traveller.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_name_traveller.setFont(new Font("Bookman Old Style", Font.PLAIN, 15));
				lbl_name_traveller.setBounds(137, 10, 355, 30);
				panel_traveller.add(lbl_name_traveller);
			
				lbl_icon_traveller = new JLabel("");
				lbl_icon_traveller.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_icon_traveller.setBounds(40, 30, 60, 60);
				lbl_icon_traveller.setIcon(new ImageIcon(img_traveller));
				panel_traveller.add(lbl_icon_traveller);
				
				lbl_strength_trav = new JLabel("St\u00e4rke");
				lbl_strength_trav.setForeground(Color.WHITE);
				lbl_strength_trav.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_strength_trav.setFont(new Font("Bookman Old Style", Font.PLAIN, 13));
				lbl_strength_trav.setBounds(160, 60, 110, 15);
				panel_traveller.add(lbl_strength_trav);
				
				lbl_strength_traveller = new JLabel("Platzhalter");
				lbl_strength_traveller.setForeground(Color.WHITE);
				lbl_strength_traveller.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_strength_traveller.setFont(new Font("Bookman Old Style", Font.PLAIN, 11));
				lbl_strength_traveller.setBounds(160, 86, 110, 15);
				panel_traveller.add(lbl_strength_traveller);
				
				lbl_health_trav = new JLabel("Leben");
				lbl_health_trav.setForeground(Color.WHITE);
				lbl_health_trav.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_health_trav.setFont(new Font("Bookman Old Style", Font.PLAIN, 13));
				lbl_health_trav.setBounds(350, 60, 110, 15);
				panel_traveller.add(lbl_health_trav);
				
				lbl_health_traveller = new JLabel("Platzhalter");
				lbl_health_traveller.setForeground(Color.WHITE);
				lbl_health_traveller.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_health_traveller.setFont(new Font("Bookman Old Style", Font.PLAIN, 11));
				lbl_health_traveller.setBounds(350, 86, 110, 15);
				panel_traveller.add(lbl_health_traveller);
				
			/*
			 * Panel für die Informationen des Charakters "Walküre"
			 * */
			panel_valkyrie = new JPanel();
			panel_valkyrie.setBackground(new Color(0, 31, 31));
			panel_valkyrie.setBorder(new LineBorder(new Color(0, 0, 0)));
			panel_valkyrie.setLayout(null);
			panel_valkyrie.addMouseListener(ma);
			panel_valkyrie.setBounds(35, 349, 500, 120);
			add(panel_valkyrie);
			
				lbl_name_valkyrie = new JLabel("Platzhalter");
				lbl_name_valkyrie.setForeground(Color.WHITE);
				lbl_name_valkyrie.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_name_valkyrie.setFont(new Font("Bookman Old Style", Font.PLAIN, 15));
				lbl_name_valkyrie.setBounds(137, 10, 355, 30);
				panel_valkyrie.add(lbl_name_valkyrie);
				
				lbl_icon_valkyrie = new JLabel("");
				lbl_icon_valkyrie.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_icon_valkyrie.setBounds(40, 30, 60, 60);
				lbl_icon_valkyrie.setIcon(new ImageIcon(img_valkyrie));
				panel_valkyrie.add(lbl_icon_valkyrie);
				
				lbl_strength_val = new JLabel("St\u00e4rke");
				lbl_strength_val.setForeground(Color.WHITE);
				lbl_strength_val.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_strength_val.setFont(new Font("Bookman Old Style", Font.PLAIN, 13));
				lbl_strength_val.setBounds(160, 60, 110, 15);
				panel_valkyrie.add(lbl_strength_val);
				
				lbl_strength_valkyrie = new JLabel("Platzhalter");
				lbl_strength_valkyrie.setForeground(Color.WHITE);
				lbl_strength_valkyrie.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_strength_valkyrie.setFont(new Font("Bookman Old Style", Font.PLAIN, 11));
				lbl_strength_valkyrie.setBounds(160, 86, 110, 15);
				panel_valkyrie.add(lbl_strength_valkyrie);
				
				lbl_health_val = new JLabel("Leben");
				lbl_health_val.setForeground(Color.WHITE);
				lbl_health_val.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_health_val.setFont(new Font("Bookman Old Style", Font.PLAIN, 13));
				lbl_health_val.setBounds(350, 60, 110, 15);
				panel_valkyrie.add(lbl_health_val);
				
				lbl_health_valkyrie = new JLabel("Platzhalter");
				lbl_health_valkyrie.setForeground(Color.WHITE);
				lbl_health_valkyrie.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_health_valkyrie.setFont(new Font("Bookman Old Style", Font.PLAIN, 11));
				lbl_health_valkyrie.setBounds(350, 86, 110, 15);
				panel_valkyrie.add(lbl_health_valkyrie);
	}
	
/********************************** Funktionen **********************************/
	
	/*
	 * Importieren der im Vorfeld festgelegten Eigenschaften jedes Characters
	 * */
	public void showcharacter_mage(String name, String strength, String  health) {
		lbl_name_mage.setText(name);
		lbl_strength_mage.setText(strength);
		lbl_health_mage.setText(health);
	}
	
	public void showcharacter_traveller(String name, String strength, String  health) {
		lbl_name_traveller.setText(name);
		lbl_strength_traveller.setText(strength);
		lbl_health_traveller.setText(health);
	}
	
	public void showcharacter_valkyrie(String name, String strength, String  health) {
		lbl_name_valkyrie.setText(name);
		lbl_strength_valkyrie.setText(strength);
		lbl_health_valkyrie.setText(health);
	}
}