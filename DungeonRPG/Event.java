/*
 * Inhalt / Content	:	EventKlasse
 * 
 * Autor  / Creator	:	Felix Ohlendorf
 * 
 * Datum  / Date	:	15.05.2020
 * 
 * Version: 1.00
 * 
 * */
import java.io.Serializable;

public class Event implements Serializable{
	/*
	 * Attribute
	 * */
	private String name;
	private String exercise;
	private String answer;
	private String help;
	
	/*
	 * Konstruktor
	 * */
	Event(String name, String exercise, String answer, String help){
		this.exercise = exercise;
		this.name = name;
		this.answer = answer;
		this.help = help;
	}
	
	/*
	 * Getter und Setter
	 * */
	public void setName(String name){ this.name = name; }
	public void setExercise(String exercise){ this.exercise = exercise; }
	public void setAnswer(String answer){ this.answer = answer; }
	public void setHelp(String help){ this.help = help; }
	public String getName(){ return this.name; }
	public String getExercise(){ return this.exercise; }
	public String getAnswer(){ return this.answer; }
	public String getHelp(){ return this.help; }
}