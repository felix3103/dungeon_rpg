/*
 * Inhalt / Content	:     lesen und schreiben für das Auslesen der gespeicherten Event- und Charakterinformationen / read and write for the readout of the saved event- and character-information
 *
 * Author / Creator	:     Georg Reichelt
 *
 * Datum / Date		:     24.06.2020
 *
 * Version	        :     1.00
 *
 * */

import java.io.*;

public class Load {
    /*
     * Schreibt Event in "loadEvent.dat"
     * */
    public static void writeEvent(Event event){
        Event de = event;

        try {
            File datei = new File("status/Event.dat");
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(datei));

            oos.writeObject(de);
            oos.flush();

            oos.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /*
     * Liest aus "loadEvent.dat" das gespeicherte Event
     * */
    public static Event readEvent(){
        Event event = null;
        try {
            File datei = new File("status/Event.dat");

            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(datei));
            Object rawObject = ois.readObject();
            ois.close();

            Event readEvent = (Event) rawObject;

            event = readEvent;

        }catch(Exception e){

            e.printStackTrace();
        }
        return event;
    }

    /*
     * Schreibt Character in "loadEvent.dat"
     * */
    public static void writeCharacter(Character.Hero hero){
        Character.Hero dc = hero;

        try {
            File datei = new File("status/Character.dat");
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(datei));

            oos.writeObject(dc);
            oos.flush();

            oos.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /*
     * Liest aus "loadCharacter.dat" den gespeicherten Charakter
     * */
    public static Character.Hero readCharacter(){
        Character.Hero hero = null;
        try {
            File datei = new File("status/Character.dat");

            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(datei));
            Object rawObject = ois.readObject();
            ois.close();

            Character.Hero readCharacter = (Character.Hero) rawObject;

            hero = readCharacter;

        }catch(Exception e){
            e.printStackTrace();
        }
        return hero;
    }
}
