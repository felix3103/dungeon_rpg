/* 
 * Inhalt / Content	: 	Einstellungen der GUI vom Hauptmenü / File of the Game Menu GUI
 * 
 * Autor  / Creator	: 	Janek Hickethier
 * 
 * Datum  / Date	: 	28.06.2020
 * 
 * Version			: 	1.00
 * 	
 * */

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JOptionPane;
import javax.swing.border.CompoundBorder;


/*
 * Erstelle die Klasse "Menu"
 * */
public class Menu extends JFrame {

	/*
 	 * Attribute
 	 * */
	public JPanel panel_load;
	public JPanel panel_new;
	public JPanel panel_exit;

	private JPanel contentPane;
	private JPanel panel_Menu;
	private JLabel lbl_logo;
	private JLabel lbl_title;
	private JLabel lbl_load;
	private JLabel lbl_new;
	private JLabel lbl_exit;
	private JPanel panel_welcome;
	private JLabel lbl_welcome;
	private JLabel lbl_intro1;
	private JLabel lbl_intro2;
	private JLabel lbl_intro3;
	private JLabel lbl_intro4;
	private JLabel lbl_author1;
	private JLabel lbl_author2;
	private JLabel lbl_author3;
	private JLabel lbl_ ;
	private Image imglogo = new ImageIcon(Menu.class.getResource("/images/dragon.png")).getImage().getScaledInstance(90, 90, Image.SCALE_SMOOTH);
	
	/*
	 * Erstellen des "Menu"-Fensters
	 * */
	public Menu(MouseAdapter ma) {
		
		/*
		 * GUI-Informationen über das ganze Fenster
		 * */
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/images/dragon.png")));
		setBounds(100, 100, 800, 500);
		setUndecorated(true);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setForeground(Color.BLACK);
		contentPane.setBackground(new Color(0, 51, 51));
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		/*
		 * Das Panel dient als Menü-Fenster für das Spiel
		 * */
		panel_Menu = new JPanel();
		panel_Menu.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_Menu.setBackground(new Color(255, 222, 173));
		panel_Menu.setBounds(70, 1, 230, 498);
		contentPane.add(panel_Menu);
		panel_Menu.setLayout(null);
		
			/*
			 * Zeigt das Icon
			 * */
			lbl_logo = new JLabel("");
			lbl_logo.setHorizontalAlignment(SwingConstants.CENTER);
			lbl_logo.setBounds(10, 11, 210, 129);
			lbl_logo.setIcon(new ImageIcon(imglogo));
			panel_Menu.add(lbl_logo);
			
			/*
			 * Zeigt den Titel
			 * */
			lbl_title = new JLabel("Dungeon RPG");
			lbl_title.setFont(new Font("Bookman Old Style", Font.PLAIN, 20));
			lbl_title.setHorizontalAlignment(SwingConstants.CENTER);
			lbl_title.setBounds(10, 151, 210, 40);
			panel_Menu.add(lbl_title);
			
			/*
			 *  Klickbares Panel um ein Spiel zu laden
			 * */
			panel_load = new JPanel();
			panel_load.setBorder(new LineBorder(new Color(222, 184, 135)));
			panel_load.setBackground(new Color(210, 180, 140));
			panel_load.setBounds(1, 206, 228, 40);
			panel_Menu.add(panel_load);
			panel_load.setLayout(null);
			panel_load.addMouseListener(ma);
			
				lbl_load = new JLabel("Laden");
				lbl_load.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
				lbl_load.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_load.setBounds(10, 11, 208, 18);
				panel_load.add(lbl_load);

			/*
			 * Klickbares Panel um ein neues Spiel zu starten
			 * */
			panel_new = new JPanel();
			panel_new.setBounds(1, 257, 228, 40);
			panel_new.setFont(new Font("Arial", Font.BOLD, 12));
			panel_new.setBorder(new LineBorder(new Color(222, 184, 135)));
			panel_new.setBackground(new Color(210, 180, 140));
			panel_new.setLayout(null);
			panel_new.addMouseListener(ma);
			panel_Menu.add(panel_new);
			
				lbl_new = new JLabel("Neues Spiel");
				lbl_new.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
				lbl_new.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_new.setBounds(10, 11, 208, 18);
				panel_new.add(lbl_new);
				
			/*
			 * Klickbares Panel um das Fenster zu schließen
			 * */
			panel_exit = new JPanel();
			panel_exit.setBorder(new LineBorder(new Color(222, 184, 135)));
			panel_exit.setBackground(new Color(210, 180, 140));
			panel_exit.setBounds(1, 410, 228, 40);
			panel_exit.setLayout(null);
			panel_exit.addMouseListener(ma);
			panel_Menu.add(panel_exit);
				
				lbl_exit = new JLabel("Verlassen");
				lbl_exit.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
				lbl_exit.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_exit.setBounds(10, 11, 208, 18);
				panel_exit.add(lbl_exit);		
		
		/*
		 * Zeigt Informationstext über das Spiel
		 * */
		panel_welcome = new JPanel();
		panel_welcome.setBorder(new CompoundBorder());
		panel_welcome.setBackground(new Color(20, 71, 71));
		panel_welcome.setBounds(367, 44, 370, 411);
		contentPane.add(panel_welcome);
		panel_welcome.setLayout(null);
		
			lbl_welcome = new JLabel("Willkommen");
			lbl_welcome.setForeground(Color.BLACK);
			lbl_welcome.setHorizontalAlignment(SwingConstants.CENTER);
			lbl_welcome.setFont(new Font("Rockwell", Font.PLAIN, 36));
			lbl_welcome.setBounds(23, 41, 318, 88);
			panel_welcome.add(lbl_welcome);
			
			lbl_intro1 = new JLabel("Dieses Spiel ist innerhalb eines Semesters an der");
			lbl_intro1.setForeground(Color.BLACK);
			lbl_intro1.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 12));
			lbl_intro1.setHorizontalAlignment(SwingConstants.CENTER);
			lbl_intro1.setBounds(10, 180, 350, 22);
			panel_welcome.add(lbl_intro1);
			
			lbl_intro2 = new JLabel("Berufsakademie Dresden von einer Gruppe Studenten");
			lbl_intro2.setForeground(Color.BLACK);
			lbl_intro2.setHorizontalAlignment(SwingConstants.CENTER);
			lbl_intro2.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 12));
			lbl_intro2.setBounds(10, 199, 350, 22);
			panel_welcome.add(lbl_intro2);
			
			lbl_intro3 = new JLabel("des Studiengangs Medieninformatik entstanden.");
			lbl_intro3.setForeground(Color.BLACK);
			lbl_intro3.setHorizontalAlignment(SwingConstants.CENTER);
			lbl_intro3.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 12));
			lbl_intro3.setBounds(10, 218, 350, 22);
			panel_welcome.add(lbl_intro3);
			
			lbl_intro4 = new JLabel("Wir wünschen viel Spaß und Erfolg beim Spielen!");
			lbl_intro4.setForeground(Color.BLACK);
			lbl_intro4.setFont(new Font("Rockwell", Font.PLAIN, 14));
			lbl_intro4.setHorizontalAlignment(SwingConstants.CENTER);
			lbl_intro4.setBounds(10, 269, 350, 47);
			panel_welcome.add(lbl_intro4);
			
			lbl_author1 = new JLabel("Dank gilt auch den Designern der Icons:");
			lbl_author1.setFont(new Font("Rockwell", Font.PLAIN, 10));
			lbl_author1.setForeground(Color.BLACK);
			lbl_author1.setHorizontalAlignment(SwingConstants.CENTER);
			lbl_author1.setBounds(10, 366, 350, 14);
			panel_welcome.add(lbl_author1);
			
			lbl_author2 = new JLabel("Character Icons: Freepik");
			lbl_author2.setFont(new Font("Rockwell", Font.PLAIN, 10));
			lbl_author2.setForeground(Color.BLACK);
			lbl_author2.setHorizontalAlignment(SwingConstants.CENTER);
			lbl_author2.setBounds(10, 376, 350, 14);
			panel_welcome.add(lbl_author2);
			
			lbl_author3 = new JLabel("Drachen Icon: Nikita Golubev");
			lbl_author3.setFont(new Font("Rockwell", Font.PLAIN, 10));
			lbl_author3.setForeground(Color.BLACK);
			lbl_author3.setHorizontalAlignment(SwingConstants.CENTER);
			lbl_author3.setBounds(10, 386, 350, 14);
			panel_welcome.add(lbl_author3);
			
		/*
		 * Dient zum Minimieren des Fensters
		 * */
		lbl_ = new JLabel("_");
		lbl_.setForeground(Color.BLACK);
		lbl_.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Menu.this.setState(Menu.ICONIFIED);
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lbl_.setForeground(Color.RED);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lbl_.setForeground(Color.BLACK);
			}
		});
		lbl_.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_.setFont(new Font("MS UI Gothic", Font.BOLD, 18));
		lbl_.setBounds(765, 0, 25, 25);
		contentPane.add(lbl_);
	}
	
/********************************** Funktionen **********************************/
	
	/*
	 * Erzeugen eines Hover_Effektes
	 * */
	public void hoverenter_load() { panel_load.setBackground(new Color(230, 200, 160));}
	public void hoverexited_load() { panel_load.setBackground(new Color(210, 180, 140));}
	
	public void hoverenter_new() { panel_new.setBackground(new Color(230, 200, 160));}
	public void hoverexited_new() { panel_new.setBackground(new Color(210, 180, 140));}
	
	public void hoverenter_exit() { panel_exit.setBackground(new Color(230, 200, 160));}
	public void hoverexited_exit() { panel_exit.setBackground(new Color(210, 180, 140));}
	
	/*
	 * Einbinden einer Nachrichten-Box / Pop-up Box
	 * */
	public void msgbox(String s){ JOptionPane.showMessageDialog(null, s); }
}