/* 
 * Inhalt / Content	: 	Einstellungen der GUI vom Hauptspiel / File of the actual Game GUI
 * 
 * Autor  / Creator	: 	Janek Hickethier
 * 
 * Datum  / Date	: 	28.06.2020
 * 
 * Version			: 	1.00
 * 	
 * */

import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.MouseAdapter;

import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTextPane;

/*
 * Erstelle die Klasse "Panel_Game"
 * */
public class Panel_Game extends JPanel {
	
	/*
 	 * Attribute
 	 * */
	public JTextPane txtpn_storytextfield;
	public JLabel lbl_usererror;
	public JTextField textField_userinput;
	public JPanel panel_ok;
	public JLabel lbl_ok;

	private JScrollPane scrollPane_storytf;
	private JPanel panel_usererror;
	private JPanel panel_userinput;

	/*
	 * Erstellen des Panels "Panel_Game"
	 * */
	public Panel_Game(MouseAdapter ma) {
		
		/*
		 * GUI-Informationen über das ganze Panel
		 * */
		setBackground(new Color(0, 51, 51));
		setBounds(0, 0, 584, 521);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setLayout(null);
			
			/*
			 * Dient dazu, wenn ein darzustellender Storytext zu lang für den vorgegebenen Rahmen ist, dass dieser trotzdem dargestellt werden kann
			 * */
			scrollPane_storytf = new JScrollPane();
			scrollPane_storytf.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane_storytf.setBorder(new LineBorder(Color.BLACK));
			scrollPane_storytf.setBounds(10, 11, 564, 372);
			add(scrollPane_storytf);
			
				/*
				 * zeigt den Text der Story
				 * */
				txtpn_storytextfield = new JTextPane();
				txtpn_storytextfield.setFont(new Font("Sitka Heading", Font.PLAIN, 13));
				txtpn_storytextfield.setForeground(Color.WHITE);
				txtpn_storytextfield.setEditable(false);
				txtpn_storytextfield.setBackground(new Color(0, 31, 31));
				txtpn_storytextfield.setMargin(new Insets(15, 15, 15, 15));
				scrollPane_storytf.setViewportView(txtpn_storytextfield);			
			
			/*
		 	 * Das Panel dient dazu den Spieler auf fehlerhafte Eingaben aufmerksam zu machen
			 * */
			panel_usererror = new JPanel();
			panel_usererror.setLayout(null);
			panel_usererror.setBorder(new EmptyBorder(0, 0, 0, 0));
			panel_usererror.setBackground(new Color(0, 51, 51));
			panel_usererror.setBounds(10, 395, 564, 40);
			add(panel_usererror);
			
				lbl_usererror = new JLabel("");
				lbl_usererror.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_usererror.setForeground(Color.WHITE);
				lbl_usererror.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 13));
				lbl_usererror.setBounds(10, 10, 545, 20);
				panel_usererror.add(lbl_usererror);			
			
			/*
			 * Das Panel dient dazu dem Spieler eingaben zu ermöglichen
			 * */
			panel_userinput = new JPanel();
			panel_userinput.setForeground(Color.BLACK);
			panel_userinput.setLayout(null);
			panel_userinput.setBorder(new LineBorder(new Color(0, 0, 0), 2));
			panel_userinput.setBackground(Color.WHITE);
			panel_userinput.setBounds(10, 451, 470, 40);
			add(panel_userinput);
			
				textField_userinput = new JTextField();
				textField_userinput.setDisabledTextColor(Color.WHITE);
				textField_userinput.setEditable(false);
				textField_userinput.setForeground(Color.BLACK);
				textField_userinput.setHorizontalAlignment(SwingConstants.CENTER);
				textField_userinput.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 12));
				textField_userinput.setColumns(10);
				textField_userinput.setBorder(new EmptyBorder(0, 0, 0, 0));
				textField_userinput.setBounds(10, 10, 450, 20);
				panel_userinput.add(textField_userinput);
			
			/*
			 * Das Panel dient dazu die Eingaben des Spielers zu bestätigen bzw. um in der Story weiter zu gehen
			 * */
			panel_ok = new JPanel();
			panel_ok.setLayout(null);
			panel_ok.setBorder(new LineBorder(new Color(0, 0, 0)));
			panel_ok.setBackground(new Color(0, 31, 31));
			panel_ok.setBounds(485, 451, 89, 40);
			panel_ok.addMouseListener(ma);
			add(panel_ok);
			
				lbl_ok = new JLabel("Weiter");
				lbl_ok.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_ok.setForeground(Color.WHITE);
				lbl_ok.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
				lbl_ok.setBorder(new EmptyBorder(0, 0, 0, 0));
				lbl_ok.setBounds(10, 13, 69, 14);
				panel_ok.add(lbl_ok);
	}
}