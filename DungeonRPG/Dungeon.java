/*
 * Inhalt / Content	: 	DungeonRPG Controller
 * 
 * Autor  / Creator	:	Felix Ohlendorf
 * 
 * Datum  / Date	:	30.05.2020
 * 
 * Version			:	1.00
 * 
 * */
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
/*
 * Obeklasse "DungeonRPG"
 * 
 * Die Oberklasse besteht aus Funktionen die die Logik einiger Ereignisse darstellen.
 * Außerdem sind hier alle Controller des Spieles hinterlegt, in Form von Unterklassen.
 * 
 * */
public class Dungeon{
	/*
	 * Attribute
	 * 
	 * Hier sind die Spielvariablen hinterlegt.
	 * Unter anderem gewählte Charakter (hero) das aktuelle Event (active) und die Geschichte (story).
	 * 
	 * */
	private static Event active = null;
	private static Character.Hero hero = null;
	private static Content content;
	private static Story story;
	private static Character.Enemie enemie;
	private static int trie = 1;
	private static int maxhealth = 25;
	/*
	 * Hilfsfunktionen
	 * 
	 * */
	
	/*
	 * "loadEvent" soll das letzte Event laden, wenn das nicht möglich ist, wird der Spieler an den Anfang der Story gesetzt.
	 * Dabei wird der Spieler darüber informiert, ob das Laden erfolgreich war oder nicht.
	 * 
	 * */
	static void loadEvent(Event event, MainGame view){
		String message;
		if(event == null){
			message = "Letzter Stand konnte nicht geladen werden.";
			active = story.getFirst();
		}else{
			message = "Letzter Stand wurde erfolgreich geladen.";
			active = story.get(story.searchEvent(event.getName()));
			if (active == story.getLast()){
				message = "Speicherstand ist nicht verfügbar.";
			}
		}
		view.msgbox(message);
		return;
	}
	/*
	 * "initEvent" initialisiert das aktuelle Event.
	 * Dabei wird geprüft, an welcher Position der Story sich das Event befindet.
	 * Außerdem wird geprüft ob es eine Antwort gibt, sollte die Antwort leer sein,
	 * handelt es sich um ein Kampfevent. D.h. es wird ein Gegner erzeugt.
	 * 
	 * */
	static void initEvent(MainGame view) {
		if(active == story.getLast() || active == story.getFirst()){
			view.OktoNext();
		}else{
			if(active.getAnswer().isEmpty()){
				enemie = Character.Enemie.initEnemie(active.getName());
			}
			view.NexttoOk();
		}
		trie = 1;
		view.setStoryText(active.getExercise());
		return;
	}
	/*
	 * "loadCharacter" soll den letzten Charakter laden.
	 * Wenn das nicht möglich ist, soll der Spieler einen neuen Charakter auswählen.
	 * Dabei wird der Spieler darüber informiert, ob das Laden erfolgreich war.
	 * 
	 * */
	static void loadCharacter(Character.Hero character, MainGame view){
		String message;
		if(character == null){
			message = "Character konnte nicht geladen werden.";
			view.pcharw();
		}else{
			hero = character;
			message = "Willkommen zurück, " + hero.getName();
			view.pgame();
		}
		view.msgbox(message);
		return;
	}
	/*
	 * "logicFight" ist die Logik für Kampfevents. Dabei muss so oft "kampf" eingegeben werden, bis der Gegner besiegt ist.
	 * Dabei wird dem Spieler angeziegt, wie viel Leben er und der Gegner noch haben.
	 * 
	 * */
	static void logicFight(MainGame view){
		if(hero.getHealth() != 0 && enemie.getHealth() != 0){
			if(view.getUserInput().toLowerCase().equals("kampf")){
				enemie.setHealth(enemie.getHealth() - hero.getStrength());
			}
			hero.setHealth(hero.getHealth() - enemie.getStrength());
			if(enemie.getHealth() <= 0) enemie.setHealth(0);
			if(hero.getHealth() <= 0) hero.setHealth(0);
			view.setUserError("Du hast noch " + hero.getHealth() + " und der Gegner " + enemie.getHealth() + " Leben");
		}
		if(enemie.getHealth() <= 0){
			hero.setHealth(maxhealth);
			winning(view);
		}
		if(hero.getHealth() <= 0){
			gameOver(view);
		}
		if(hero.getHealth() <= 0 || enemie.getHealth() <= 0) {
			view.OktoNext();
		}
		return;
	}
	/*
	 * "logicQuiz" ist die Logik für Wissensevents. Dabei muss die Eingabe im "UserInput" mit der Antwort aus dem Event übereinstimmen.
	 * Dabei hat der Spieler zwei Versuche. Wenn der Spieler nur noch einen Versuch hat, wird ihm das angezeigt.
	 * 
	 * */
	static void logicQuiz(MainGame view){
		if(active.getAnswer().toLowerCase().equals(view.getUserInput().toLowerCase().trim())){
			winning(view);
		}else {
			if(trie == 0){
				gameOver(view);
			}else{
				view.setUserError("Du hast noch einen Versuch.");
				trie--;
			}
		}
		return;
	}
	/*
	 * "pushHero" soll das angegebene Item als Belohnung dem Helden überreichen.
	 * Dabei sollen die Attribute des Items auf die des Heros addiert werden.
	 * 
	 * */
	static void pushHero(Item present){
		hero.setStrength(hero.getStrength() + present.getStrenght());
		int a = (hero.getHealth() + present.getHealth());
		if(a <= 100) hero.setHealth(a);
		return;
	}
	/*
	 * "winnig" ist die Logik für das gewinnen eines Events.
	 * Dabei wird ein zufälliges Item erzeugt und übergeben.
	 * Dem Spieler wird der Name des Items angezeigt.
	 * 
	 * */
	static void winning(MainGame view){
		Item present = story.randomObjekt();
		pushHero(present);
		maxhealth = hero.getHealth();
		view.OktoNext();
		view.msgbox("Du bekommst ein Item: " + present.getName());
		return;
	}
	/*
	 * "gameOver" ist die Logik für das verlieren eines Events.
	 * Dabei wird der Spieler an den Anfang zurück gesetzt.
	 * Und der Charakter auf den Urzustand zurückgesetzt.
	 * 
	 * */
	static void gameOver(MainGame view){
		active = story.getFirst();
		maxhealth = 25;
		hero.setHealth(maxhealth);
		view.setStoryText(active.getExercise());
		view.ClearUserError();
		view.ClearUserInput();
		view.OktoNext();
		view.msgbox("Du hast verloren. Und kehrst zur\u00fcck zum Anfang, als " + hero.getName());
		return;
	}


	/*
	 * Controller	:	"StartMenu"
	 * View			:	"Menu"
	 *
	 * */
	static class StartMenu extends MouseAdapter{
		/*
		 * Attribute
		 * */
		static Menu view;

		public void mouseClicked(MouseEvent me){
			/*
			 * Beim Klick im "Menu" soll der "UserInput" und der "UserEror" gelöscht werden.
			 * Danach soll das "MainGame" sichtbar sein und das "Menu" verschwinden.
			 * 
			 * */
			Game.view.ClearUserInput();
			Game.view.ClearUserError();
			StartMenu.view.setVisible(false);
			Game.view.setVisible(true);
			/*
			 * In Abhängigkeit der Quelle des Events soll das Spiel geladen werden.
			 * 
			 * Wenn das Spiel neu geladen wird, soll er am Anfang beginnen und sich einen Charakter aussuchen.
			 * 
			 * */
			if(me.getSource() == view.panel_new){
				Game.view.pcharw();
				active = story.getFirst();
			}
			/*
			 * Beim Laden wird die Lade Funktionen aufgerufen.
			 * 
			 * */
			if(me.getSource() == view.panel_load){
				loadEvent(Load.readEvent(), Game.view);
				loadCharacter(Load.readCharacter(), Game.view);
			}
			/*
			 * Beim Exit soll das Spiel verlassen werden.
			 * 
			 * */
			if(me.getSource() == view.panel_exit){
				System.exit(0);
			}
			/*
			 * Am Ende soll das Event initialisiert werden, wenn das Spiel nicht Verlassen wird.
			 * 
			 * */
			initEvent(Game.view);
		}
		/*
		 * In "mouseEntered()" und "mouseExited()" sind die Hovereffekte hinterlegt.
		 * 
		 * */
		public void mouseEntered(MouseEvent me){
			if(me.getSource() == view.panel_new){
				view.hoverenter_new();
			}
			if(me.getSource() == view.panel_load){
				view.hoverenter_load();
			}
			if(me.getSource() == view.panel_exit){
				view.hoverenter_exit();
			}
		}

		public void mouseExited(MouseEvent me){
			if(me.getSource() == view.panel_new){
				view.hoverexited_new();
			}
			if(me.getSource() == view.panel_load){
				view.hoverexited_load();
			}
			if(me.getSource() == view.panel_exit){
				view.hoverexited_exit();
			}
		}
	}

	/*
	 * Controller	:	"Game"
	 * View			:	"MainGame"
	 * */
	static class Game extends MouseAdapter{
		/*
		 * Attribute
		 * */
		static MainGame view;

		public void mouseClicked(MouseEvent me){
			/*
			 * Beim Klicken auf "Speichern und Verlassen" wird der gewählte Held und das aktuelle Event dem Model übergeben.
			 * Der Model soll den Stand speichern.
			 * Danach soll der View ins Hauptmenü wechseln.
			 * 
			 * */
			if(me.getSource() == view.panel_saveandexit){
				if(active != story.getFirst() && active != story.getLast()){
					if(Game.view.check_ok()){
						Load.writeEvent(active);
					}
					if(Game.view.check_next()){
						Load.writeEvent(story.forward(active));
					}
					Load.writeCharacter(hero);
					Game.view.setVisible(false);
					StartMenu.view.setVisible(true);
				}
			}
		}
		/*
		 * In "mouseEntered()" und "mouseExited()" sind die Hovereffekte hinterlegt.
		 * Dabei soll der Button nur gedrückt werden,
		 * wenn sich das Event zwischen Anfang und Ende befindet.
		 * 
		 * */
		public void mouseEntered(MouseEvent me){
			if(me.getSource() == view.panel_saveandexit){
				if(active != story.getFirst() && active != story.getLast()){
					view.hoverenter_saveandexit();
				}
			}
			if(me.getSource() == view.panel_help) {
				if(active != story.getFirst() && active != story.getLast()){
					view.hoverenter_help();
					view.setStoryText(active.getHelp());
				}
			}
		}

		public void mouseExited(MouseEvent me){
			if(me.getSource() == view.panel_saveandexit){
				if(active != story.getFirst() && active != story.getLast()){
					view.hoverexited_saveandexit();
				}
			}
			if(me.getSource() == view.panel_help) {
				if(active != story.getFirst() && active != story.getLast()){
					view.hoverexited_help();
					view.setStoryText(active.getExercise());
				}
			}
		}
	}

	/*
	 * Controller	:	"PlayGame"
	 * View			:	"MainGame(Panel_Game)"
	 * */
	static class PlayGame extends MouseAdapter{

		public void mouseClicked(MouseEvent me){
			/*
			 * Im Spiel kann der Spieler auf einen Button (Panel) klicken der entweder "Weiter" angibt oder "Ok".
			 * 
			 * */
			if(me.getSource() == Game.view.panelGame.panel_ok){
				/*
				 * Es wird geprüft welchen Zustand der Button (Panel) besitzt mitttels einer Funktion des Views.
				 * 
				 * */
				if(Game.view.check_ok()) {
					/*
					 * Wenn er "Ok" ist wird geprüft, ob die Antwort leer ist.
					 * Ist die Antwort leer ist es ein Kampfevent, sonst ein Wissensevent.
					 * 
					 * Je nach dem wird die jeweilige Event Logik aufgerufen.
					 * 
					 * */
					if(active != story.getLast() && active != story.getFirst()){
						if(active.getAnswer().isEmpty()){
							logicFight(Game.view);
						}else{
							logicQuiz(Game.view);
						}
					}
					Game.view.ClearUserInput();
				/*
				 * Wenn er "Weiter" ist, wird geprüft, wo sich das Event befindet.
				 * 
				 * */
				}else if(Game.view.check_next()){
					/*
					 * Wenn das Event am Ende der Story ist, soll ein Glückwunsch ausgegeben werden.
					 * Anschließend soll der Spieler zurück ins Hauptmenu kommen.
					 * Ansonsten soll das nächste Event geladen und Initialisiert werden.
					 * 
					 * */
					if(active == story.getLast()){
						Game.view.msgbox("Ich gratuliere dir, du hast die Krone des Königs gerettet.");
						Game.view.setVisible(false);
						StartMenu.view.setVisible(true);
					}else{
						active = story.forward(active);
					}
					Game.view.ClearUserError();
					initEvent(Game.view);
				}
			}
		}
		/*
		 * In "mouseEntered()" und "mouseExited()" sind die Hovereffekte hinterlegt.
		 * 
		 * */
		public void mouseEntered(MouseEvent me){
			if(me.getSource() == Game.view.panelGame.panel_ok){
				Game.view.content_hoverenter_pok();
			}
		}

		public void mouseExited(MouseEvent me) {
			if(me.getSource() == Game.view.panelGame.panel_ok){
				Game.view.content_hoverexited_pok();
			}
		}
	}

	/*
	 * Controller	:	"ChoiceCharacter"
	 * View			:	"MainGame(Panel_Charw)"
	 * */
	static class ChoiceCharacter extends MouseAdapter{
		/*
		 * In diesem Controller soll der gewählte Charakter als Hauptcharakter gesetzt werden.
		 * 
		 * */
		public void mouseClicked(MouseEvent me){
			if(me.getSource() == Game.view.panelCharw.panel_mage){
				hero = Character.Hero.initHeros("Magier");
			}
			if(me.getSource() == Game.view.panelCharw.panel_traveller){
				hero = Character.Hero.initHeros("Reisender");
			}
			if(me.getSource() == Game.view.panelCharw.panel_valkyrie){
				hero = Character.Hero.initHeros("Walküre");
			}
			Game.view.pgame();
			Game.view.setStoryText(active.getExercise());
		}
		/*
		 * In "mouseEntered()" und "mouseExited()" sind die Hovereffekte hinterlegt.
		 * 
		 * */
		public void mouseEntered(MouseEvent me) {
			if(me.getSource() == Game.view.panelCharw.panel_mage){
				Game.view.content_hoverenter_pmage();
			}
			if(me.getSource() == Game.view.panelCharw.panel_traveller){
				Game.view.content_hoverenter_ptrav();
			}
			if(me.getSource() == Game.view.panelCharw.panel_valkyrie){
				Game.view.content_hoverenter_pvalk();
			}
		}

		public void mouseExited(MouseEvent me) {
			if(me.getSource() == Game.view.panelCharw.panel_mage){
				Game.view.content_hoverexited_pmage();
			}
			if(me.getSource() == Game.view.panelCharw.panel_traveller){
				Game.view.content_hoverexited_ptrav();
			}
			if(me.getSource() == Game.view.panelCharw.panel_valkyrie){
				Game.view.content_hoverexited_pvalk();
			}
		}
	}
	/*
	 * In der Mainfunktion wird das Spiel initialisiert und gestartet.
	 * 
	 * */
	public static void main(String[] args){

		content = Content.initStory();
		/*
		 * Es wird vor dem Beginn des Spieles geprüft, ob das Laden der Story erfolgreich war.
		 * Wenn das nicht der Fall ist, wird der Spieler darüber informiert.
		 * 
		 * */
		if(content.name[0] != null){
			Dungeon.story = new Story();
			Dungeon.story.initEvents(content.name, content.exercise, content.answer, content.iqplus);
			Dungeon.StartMenu.view = new Menu(new Dungeon.StartMenu());
			Dungeon.Game.view = new MainGame(new Dungeon.Game(), new Dungeon.PlayGame(), new Dungeon.ChoiceCharacter());
			Dungeon.Game.view.showcharacter_mage(Character.Hero.initHeros("Magier"));
			Dungeon.Game.view.showcharacter_traveller(Character.Hero.initHeros("Reisender"));
			Dungeon.Game.view.showcharacter_valkyrie(Character.Hero.initHeros("Walk\u00fcre"));
			Dungeon.StartMenu.view.setVisible(true);
		}else{
			Dungeon.StartMenu.view = new Menu(new Dungeon.StartMenu());
			Dungeon.StartMenu.view.msgbox("Es konnte kein Event initalisiert werden");
		}
	}
}